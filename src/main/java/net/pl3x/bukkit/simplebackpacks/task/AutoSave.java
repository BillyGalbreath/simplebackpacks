package net.pl3x.bukkit.simplebackpacks.task;

import net.pl3x.bukkit.simplebackpacks.configuration.PlayerConfig;
import org.bukkit.scheduler.BukkitRunnable;

public class AutoSave extends BukkitRunnable {
    @Override
    public void run() {
        for (PlayerConfig pConfig : PlayerConfig.getConfigs()) {
            if (!pConfig.isDirty()) {
                continue; // no changes to save
            }

            // save changes to disk
            pConfig.save();
        }
    }
}
