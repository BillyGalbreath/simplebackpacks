package net.pl3x.bukkit.simplebackpacks.commands;

import net.pl3x.bukkit.simplebackpacks.Backpack;
import net.pl3x.bukkit.simplebackpacks.SimpleBackpacks;
import net.pl3x.bukkit.simplebackpacks.configuration.Config;
import net.pl3x.bukkit.simplebackpacks.configuration.Lang;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

public class CmdBackpack implements TabExecutor {
    private final SimpleBackpacks plugin;

    public CmdBackpack(SimpleBackpacks plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 1 && args[0].equalsIgnoreCase("reload")) {
            if (!sender.hasPermission("backpack.reload")) {
                Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
                return true;
            }

            Config.reload(plugin);
            Lang.reload(plugin);

            Lang.send(sender, Lang.RELOAD
                    .replace("{plugin}", plugin.getName())
                    .replace("{version}", plugin.getDescription().getVersion()));
            return true;
        }

        if (!(sender instanceof Player)) {
            Lang.send(sender, Lang.PLAYER_COMMAND);
            return true;
        }

        if (!sender.hasPermission("backpack.use")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        Player player = (Player) sender;
        OfflinePlayer target;

        if (args.length > 0) {
            if (!sender.hasPermission("backpack.use.other")) {
                Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
                return true;
            }
            //noinspection deprecation
            target = Bukkit.getOfflinePlayer(args[0]);
            if (target == null) {
                Lang.send(sender, Lang.USER_NOT_FOUND);
                return true;
            }
        } else {
            target = Bukkit.getOfflinePlayer(player.getUniqueId());
        }

        Backpack backpack = plugin.getBackpackManager().getOrCreateBackpack(target);
        if (backpack == null) {
            Lang.send(sender, Lang.BACKPACK_NOT_FOUND);
            return true;
        }

        player.openInventory(backpack.getInventory());
        Lang.send(sender, Lang.BACKPACK_OPENED);
        return true;
    }
}
