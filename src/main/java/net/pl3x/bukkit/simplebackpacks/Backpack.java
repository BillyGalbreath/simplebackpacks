package net.pl3x.bukkit.simplebackpacks;

import net.pl3x.bukkit.simplebackpacks.configuration.PlayerConfig;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/**
 * Represents a player Backpack
 */
public class Backpack {
    private final SimpleBackpacks plugin;
    private final UUID owner;
    private final Inventory inventory;
    private final Inventory lastInventory;

    public Backpack(SimpleBackpacks plugin, UUID owner, Inventory inventory) {
        this.plugin = plugin;
        this.owner = owner;
        this.inventory = inventory;
        this.lastInventory = Bukkit.createInventory(null, inventory.getSize(), inventory.getTitle());
    }

    /**
     * Get UUID of owner of backpack
     *
     * @return UUID of backpack owner
     */
    public UUID getOwner() {
        return owner;
    }

    /**
     * Check if UUID is owner of backpack
     *
     * @param uuid UUID to check
     * @return True if owner
     */
    public boolean isOwner(UUID uuid) {
        return getOwner().equals(uuid);
    }

    /**
     * Get inventory of backpack
     *
     * @return Inventory of backpack
     */
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * Get the number of rows in backpack
     *
     * @return Number of rows in backpack
     */
    public int getSize() {
        return getInventory().getSize() / 9;
    }

    /**
     * Detect any changes in the backpack contents
     *
     * @return True if backpack has changed
     */
    public boolean hasChanged() {
        for (int slot = 0; slot < inventory.getSize(); slot++) {
            ItemStack current = inventory.getItem(slot);
            ItemStack last = lastInventory.getItem(slot);
            if (current == null && last == null) {
                continue; // both null, no change.
            }
            if (current == null || last == null) {
                return true; // one is null. changed.
            }
            if (!current.equals(last)) {
                return true; // not same item. changed.
            }
        }
        return false; // no change.
    }

    /**
     * Clear the contents of the backpack and save
     */
    public void clear() {
        for (int slot = 0; slot < inventory.getSize(); slot++) {
            inventory.setItem(slot, null);
        }
        save();
    }

    /**
     * Save the contents of the backpack if changes were found
     */
    public void save() {
        PlayerConfig pConfig = PlayerConfig.getConfig(plugin, owner);
        for (int slot = 0; slot < inventory.getSize(); slot++) {
            ItemStack item = inventory.getItem(slot);
            lastInventory.setItem(slot, item);
            pConfig.set("backpack.item." + slot, item);
        }
        pConfig.set("backpack.last-known-size", getSize());
        pConfig.setDirty(true);
    }

    /**
     * Load the contents of the backpack
     */
    public void load() {
        PlayerConfig config = PlayerConfig.getConfig(plugin, owner);
        for (int slot = 0; slot < inventory.getSize(); slot++) {
            ItemStack item = config.getItemStack("backpack.item." + slot);
            if (item == null) {
                continue;
            }
            inventory.setItem(slot, item);
        }
    }
}
