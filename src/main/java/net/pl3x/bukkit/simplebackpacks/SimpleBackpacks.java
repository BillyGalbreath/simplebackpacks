package net.pl3x.bukkit.simplebackpacks;

import net.pl3x.bukkit.simplebackpacks.commands.CmdBackpack;
import net.pl3x.bukkit.simplebackpacks.configuration.Config;
import net.pl3x.bukkit.simplebackpacks.configuration.Lang;
import net.pl3x.bukkit.simplebackpacks.listener.PlayerListener;
import net.pl3x.bukkit.simplebackpacks.manager.BackpackManager;
import net.pl3x.bukkit.simplebackpacks.task.AutoSave;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class SimpleBackpacks extends JavaPlugin {
    private BackpackManager backpackManager;
    private AutoSave autoSave;

    public SimpleBackpacks() {
        this.backpackManager = new BackpackManager(this);
    }

    public void onEnable() {
        Config.reload(this);
        Lang.reload(this);

        Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);

        getCommand("backpack").setExecutor(new CmdBackpack(this));

        autoSave = new AutoSave();
        autoSave.runTaskTimer(this, 20, Config.AUTOSAVE_INTERVAL * 1200);

        getLogger().info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    public void onDisable() {
        if (autoSave != null) {
            autoSave.run();
            autoSave.cancel();
            autoSave = null;
        }

        if (backpackManager != null) {
            backpackManager.unloadAll();
        }

        getLogger().info(getName() + " Disabled.");
    }

    public BackpackManager getBackpackManager() {
        return backpackManager;
    }
}
