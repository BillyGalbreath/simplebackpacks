package net.pl3x.bukkit.simplebackpacks.listener;

import net.pl3x.bukkit.simplebackpacks.Backpack;
import net.pl3x.bukkit.simplebackpacks.SimpleBackpacks;
import net.pl3x.bukkit.simplebackpacks.configuration.Config;
import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

public class PlayerListener implements Listener {
    private final SimpleBackpacks plugin;

    public PlayerListener(SimpleBackpacks plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void invClose(InventoryCloseEvent event) {
        HumanEntity entity = event.getPlayer();
        if (!(entity instanceof Player)) {
            return;
        }
        Backpack backpack = plugin.getBackpackManager().getBackpack(event.getInventory());
        if (backpack == null) {
            return; // not a backpack
        }
        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            // check backpack for changes
            if (backpack.hasChanged()) {
                // save backpack
                backpack.save();
            }
        }, 1);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void invClick(InventoryClickEvent event) {
        HumanEntity entity = event.getWhoClicked();
        if (!(entity instanceof Player)) {
            return;
        }
        Backpack backpack = plugin.getBackpackManager().getBackpack(event.getInventory());
        if (backpack == null) {
            return; // not a backpack
        }
        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            // check backpack for changes
            if (backpack.hasChanged()) {
                // save backpack
                backpack.save();
            }
        }, 1);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDeath(PlayerDeathEvent event) {
        // drop backpack
        if (!Config.EMPTY_BACKPACK_ON_DEATH) {
            return;
        }

        Player player = event.getEntity();
        if (player.hasPermission("backpack.exempt.empty-on-death")) {
            return;
        }

        Backpack backpack = plugin.getBackpackManager().getBackpack(player);
        if (backpack == null) {
            return; // no backpack found
        }

        if (Config.DROP_BACKPACK_ITEMS_ON_DEATH) {
            for (ItemStack item : backpack.getInventory().getContents()) {
                if (item == null) {
                    continue;
                }
                event.getDrops().add(item.clone());
            }
        }
        backpack.clear();
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onJoin(PlayerJoinEvent event) {
        final UUID uuid = event.getPlayer().getUniqueId();
        // load backpack
        new BukkitRunnable() {
            @Override
            public void run() {
                plugin.getBackpackManager().getOrCreateBackpack(Bukkit.getOfflinePlayer(uuid));
            }
        }.runTaskLater(plugin, 5);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onQuit(PlayerQuitEvent event) {
        final UUID uuid = event.getPlayer().getUniqueId();
        // unload backpack
        new BukkitRunnable() {
            @Override
            public void run() {
                plugin.getBackpackManager().unloadBackpack(Bukkit.getOfflinePlayer(uuid));
            }
        }.runTaskLater(plugin, 20);
    }
}
