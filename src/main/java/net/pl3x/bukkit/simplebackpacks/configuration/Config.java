package net.pl3x.bukkit.simplebackpacks.configuration;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Config {
    public static String LANGUAGE_FILE = "lang-en.yml";
    public static int AUTOSAVE_INTERVAL = 5;
    public static boolean EMPTY_BACKPACK_ON_DEATH = false;
    public static boolean DROP_BACKPACK_ITEMS_ON_DEATH = false;

    public static void reload(JavaPlugin plugin) {
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");
        AUTOSAVE_INTERVAL = config.getInt("autosave-interval", 5);
        EMPTY_BACKPACK_ON_DEATH = config.getBoolean("empty-backpack-on-death", false);
        DROP_BACKPACK_ITEMS_ON_DEATH = config.getBoolean("drop-backpack-items-on-death", false);
    }
}
