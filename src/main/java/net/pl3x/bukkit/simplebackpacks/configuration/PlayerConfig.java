package net.pl3x.bukkit.simplebackpacks.configuration;

import net.pl3x.bukkit.simplebackpacks.SimpleBackpacks;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerConfig extends YamlConfiguration {
    private static final Map<UUID, PlayerConfig> configs = new HashMap<>();

    public static PlayerConfig getConfig(SimpleBackpacks plugin, UUID uuid) {
        synchronized (configs) {
            return configs.computeIfAbsent(uuid, k -> new PlayerConfig(plugin, uuid));
        }
    }

    public static Collection<PlayerConfig> getConfigs() {
        return configs.values();
    }

    public static void remove(UUID uuid) {
        synchronized (configs) {
            configs.remove(uuid);
        }
    }

    public static void removeAll() {
        synchronized (configs) {
            configs.clear();
        }
    }

    private File file = null;
    private final Object saveLock = new Object();
    private final UUID uuid;
    private boolean isDirty = false;

    private PlayerConfig(SimpleBackpacks plugin, UUID uuid) {
        super();
        this.uuid = uuid;
        file = new File(plugin.getDataFolder(), "userdata" + File.separator + uuid + ".yml");
        try {
            load(file);
        } catch (Exception ignored) {
        }
    }

    public void save() {
        if (!isDirty) {
            return;
        }
        synchronized (saveLock) {
            try {
                save(file);
                isDirty = false;
            } catch (IOException ignored) {
            }
        }
    }

    public UUID getUUID() {
        return uuid;
    }

    public boolean isDirty() {
        return isDirty;
    }

    public void setDirty(boolean dirty) {
        isDirty = dirty;
    }
}
