package net.pl3x.bukkit.simplebackpacks.configuration;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class Lang {
    public static String COMMAND_NO_PERMISSION = "&4You do not have permission for that command!";
    public static String PLAYER_COMMAND = "&4This command is only available to players.";
    public static String USER_NOT_FOUND = "&4Cannot find that user!";
    public static String BACKPACK_NOT_FOUND = "&4Cannot find backpack!";
    public static String BACKPACK_OPENED = "&dBackpack opened.";
    public static String BACKPACK_TITLE = "{player}'s Backpack";
    public static String RELOAD = "&d{plugin} v{version} reloaded.";

    public static void reload(JavaPlugin plugin) {
        String langFile = Config.LANGUAGE_FILE;
        File configFile = new File(plugin.getDataFolder(), langFile);
        plugin.saveResource(Config.LANGUAGE_FILE, false);
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        COMMAND_NO_PERMISSION = config.getString("command-no-permission", "&4You do not have permission for that command!");
        PLAYER_COMMAND = config.getString("player-command", "&4This command is only available to players.");
        USER_NOT_FOUND = config.getString("user-not-found", "&4Cannot find that user!");
        BACKPACK_NOT_FOUND = config.getString("backpack-not-found", "&4Cannot find backpack!");
        BACKPACK_OPENED = config.getString("backpack-opened", "&dBackpack opened.");
        BACKPACK_TITLE = config.getString("backpack-title", "{player}'s Backpack");
        RELOAD = config.getString("reload", "&d{plugin} v{version} reloaded.");
    }

    public static void send(CommandSender recipient, String message) {
        if (message == null) {
            return; // do not send blank messages
        }
        message = ChatColor.translateAlternateColorCodes('&', message);
        if (ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\n")) {
            recipient.sendMessage(part);
        }
    }
}
