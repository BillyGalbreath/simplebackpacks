package net.pl3x.bukkit.simplebackpacks.manager;

import net.pl3x.bukkit.simplebackpacks.Backpack;
import net.pl3x.bukkit.simplebackpacks.SimpleBackpacks;
import net.pl3x.bukkit.simplebackpacks.configuration.Lang;
import net.pl3x.bukkit.simplebackpacks.configuration.PlayerConfig;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

/**
 * Backpack Manager Util
 */
public class BackpackManager {
    private final SimpleBackpacks plugin;
    private final Set<Backpack> backpacks = new HashSet<>();

    public BackpackManager(SimpleBackpacks plugin) {
        this.plugin = plugin;
    }

    /**
     * Get someone's backpack. If one has not already been loaded into memory create a new instance.
     *
     * @param target Owner of backpack
     * @return Backpack belonging to owner
     */
    public Backpack getOrCreateBackpack(OfflinePlayer target) {
        Backpack backpack = getBackpack(target);
        if (backpack == null) {
            backpack = createBackpack(target);
        }
        if (backpack == null || backpack.getSize() != calcSize(target)) {
            backpacks.remove(backpack);
            backpack = createBackpack(target);
        }
        return backpack;
    }

    /**
     * Create a new backpack instance for someone.
     *
     * @param target Owner of backpack
     * @return Backpack belonging to owner
     */
    public Backpack createBackpack(OfflinePlayer target) {
        int rows = calcSize(target);
        if (rows == 0) {
            return null;
        }
        String name = Lang.BACKPACK_TITLE.replace("{player}", target.getName());
        Inventory inventory = Bukkit.createInventory(null, rows * 9, name);
        Backpack backpack = new Backpack(plugin, target.getUniqueId(), inventory);
        backpack.load();
        backpacks.add(backpack);
        return backpack;
    }

    /**
     * Get someone's backpack
     *
     * @param target Owner of backpack
     * @return Backpack belonging to owner
     */
    public Backpack getBackpack(OfflinePlayer target) {
        UUID targetUUID = target.getUniqueId();
        for (Backpack backpack : backpacks) {
            if (backpack.isOwner(targetUUID)) {
                return backpack;
            }
        }
        return null;
    }

    /**
     * Get backpack by inventory
     *
     * @param inv Inventory
     * @return Backpack with matching inventory
     */
    public Backpack getBackpack(Inventory inv) {
        for (Backpack backpack : backpacks) {
            if (backpack.getInventory().equals(inv)) {
                return backpack;
            }
        }
        return null;
    }

    /**
     * Unloads target's backpack from memory. Does NOT save on unload.
     *
     * @param target Owner of backpack
     */
    public void unloadBackpack(OfflinePlayer target) {
        UUID targetUUID = target.getUniqueId();
        Iterator<Backpack> iter = backpacks.iterator();
        while (iter.hasNext()) {
            if (iter.next().isOwner(targetUUID)) {
                PlayerConfig.remove(targetUUID);
                iter.remove();
            }
        }
    }

    /**
     * Unload all backpacks from memory
     */
    public void unloadAll() {
        PlayerConfig.removeAll();
        backpacks.clear();
    }

    /**
     * Calculates number of rows in backpack
     *
     * @param target Owner of backpack
     * @return Number of rows in backpack
     */
    public int calcSize(OfflinePlayer target) {
        if (target == null) {
            return 0;
        }
        int rows = 0;
        for (int i = 1; i <= 6; i++) {
            Player player = target.getPlayer();
            if (player != null && player.isOnline()) {
                if (player.hasPermission("backpack.limit." + i)) {
                    rows = i;
                }
            }
        }
        if (rows == 0) {
            rows = PlayerConfig.getConfig(plugin, target.getUniqueId()).getInt("backpack.last-known-size", 0);
        }
        return rows;
    }
}
